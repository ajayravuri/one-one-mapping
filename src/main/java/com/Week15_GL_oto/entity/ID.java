package com.Week15_GL_oto.entity;

import lombok.Getter;  
import lombok.Setter;

import javax.persistence.*;  
import java.util.UUID;

@Getter 
@Setter
@Entity
public class ID {  
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String code = UUID.randomUUID().toString();

    @OneToOne(mappedBy = "idCard")
    private Person person;

}